If you want to install them from userstyles.org and only apply them to Stylish feel free to do so without asking. You may also download the styles from this BitBucket page and manually install them. Otherwise you must ask in order to use the styles.

Do not redistribute any of these styles without my approval.

This applies to all styles.